# go-ballistic

### CLI ballistic calculator

A command line ballistic calculator that uses `gitlab.com/linuxfreak003/ballistic`


### Install

`go get gitlab.com/linuxfreak003/go-ballistic`

### Usage

`go-ballistic` will create a sqlite3 database file when it is first run.
It defaults to database.db in whatever directory it is run in.

It is used by running a various commands to do different things.

To start of you will need to run the create commands

* `go-ballistic create environment` to add an environment to the database
* `go-ballistic create rifle` to add a rifle
* `go-ballistic create load` to add a load
* `go-ballistic create scenario` to create a scenario from the other objects

Once you have added some objects to the database you can view them with the list command

`go-ballistic list [rifles, environments, loads, scenarios]`

After some scenarios have been created you can then generate tables or graphs

Some examples:
* `go-ballistic solve table 1 0 1000 25` print solution table for scenario 1 from 0-1000 yrds in 25 yard increments
* `go-ballistic graph drop 1000 1 2` graph the drop for scenarios 1 and 2 out to 1000 yrds (default saves to file graph.png, file can be specified with -o flag)
* `go-ballistic graph energy 1000 1 2` graph the energy for scenarios 1 and 2 out to 1000 yrds.
* `go-ballistic save csv 1 0 1000 25` Save solution table for scenario 1 to a csv

All commands allow the `-d` flag to specify the location of the database file.

### License

The software in the repository is licensed under the GPLv2.

### Disclaimer

THE CODE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.
