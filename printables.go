package main

import (
	"fmt"
	"strconv"

	"gitlab.com/linuxfreak003/ballistic"
)

func Round(x float64, places int) float64 {
	// Ignoring error since we are also creating the string from a float
	v, _ := strconv.ParseFloat(strconv.FormatFloat(x, 'f', places, 64), 64)
	return v
}

type ScenarioPrintable struct {
	Id            int
	Name          string
	EnvironmentId int
	RifleId       int
	LoadId        int
}

func NewScenarioPrintable(scenario *Scenario) *ScenarioPrintable {
	return &ScenarioPrintable{
		Id:            scenario.Id,
		Name:          scenario.Name,
		EnvironmentId: scenario.Env.Id,
		RifleId:       scenario.Rifle.Id,
		LoadId:        scenario.Load.Id,
	}
}

type LoadPrintable struct {
	Id             int
	BulletCaliber  float64
	BulletWeight   float64
	BulletBC       string
	BulletLength   float64
	MuzzleVelocity float64
}

func NewLoadPrintable(load *ballistic.Load) *LoadPrintable {
	return &LoadPrintable{
		Id:            load.Id,
		BulletCaliber: load.Bullet.Caliber,
		BulletWeight:  load.Bullet.Weight,
		BulletBC: fmt.Sprintf("G%d %0.3f",
			load.Bullet.BC.DragFunc,
			load.Bullet.BC.Value,
		),
		BulletLength:   load.Bullet.Length,
		MuzzleVelocity: load.MuzzleVelocity,
	}
}
