package main

import (
	"bufio"
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"strconv"

	_ "github.com/mattn/go-sqlite3"
	"github.com/rgeoghegan/tabulate"
	"github.com/spf13/cobra"
	"github.com/wcharczuk/go-chart/v2"
	"gitlab.com/linuxfreak003/ballistic"
)

func main() {
	var database string
	var bd *BallisticDatabase

	var rootCmd = &cobra.Command{
		Use:   "go-ballistic [command]",
		Short: "Go ballistic and do stuff",
	}

	rootCmd.PersistentFlags().StringVarP(&database, "database", "d", "./database.db", "file path to sqlite3 database")
	rootCmd.ParseFlags(os.Args)

	db, err := sql.Open("sqlite3", database)
	if err != nil {
		panic(err)
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Setup the Database
	bd = NewBallisticDatabase(db)

	err = bd.SetupDatabase(ctx)
	if err != nil {
		panic(err)
	}

	var versionCmd = &cobra.Command{
		Use:   "version",
		Short: "Show Version",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("go-ballistic CLI Ballistic Calculator Tool v1.0.0")
		},
	}

	var listCmd = &cobra.Command{
		Use:   "list [rifles, environments, loads, scenarios]",
		Short: "List stuff",
	}
	var createCmd = &cobra.Command{
		Use:   "create [rifle, environment, load, scenario]",
		Short: "Create items",
	}
	var solveCmd = &cobra.Command{
		Use:   "solve [table, range]",
		Short: "solve for a table or range",
	}
	var graphCmd = &cobra.Command{
		Use:   "graph [drop, wind, energy]",
		Short: "Make a graph",
	}
	var saveCmd = &cobra.Command{
		Use:   "save [csv, xlsx]",
		Short: "save solution table to file",
	}
	DatabaseListCommands(listCmd, bd)
	DatabaseCreateCommands(createCmd, bd)
	DatabaseSolveCommands(solveCmd, bd)
	AddGraphCommands(graphCmd, bd)
	AddSaveCommands(saveCmd, bd)

	rootCmd.AddCommand(versionCmd)
	rootCmd.AddCommand(listCmd)
	rootCmd.AddCommand(createCmd)
	rootCmd.AddCommand(solveCmd)
	rootCmd.AddCommand(graphCmd)
	rootCmd.AddCommand(saveCmd)
	rootCmd.Execute()
}

func AddGraphCommands(graphCmd *cobra.Command, bd *BallisticDatabase) {
	var output string
	graphCmd.PersistentFlags().StringVarP(&output, "output", "o", "graph.png", "filename for graph")
	graphCmd.ParseFlags(os.Args)

	graphCmd.AddCommand(&cobra.Command{
		Use:   "drop <max range> <list of scenario ids>",
		Short: "Graph drop for scenarios up to range",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) < 2 {
				fmt.Println("Usage: drop <max range> <list of scenario ids>")
				return
			}
			max, _ := strconv.Atoi(args[0])

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			ranges := make([]float64, max)
			for i := 0; i < max; i++ {
				ranges[i] = float64(i)
			}

			graph := chart.Chart{
				XAxis: chart.XAxis{
					Name: "Range (yrds)",
					ValueFormatter: func(v interface{}) string {
						if vf, isFloat := v.(float64); isFloat {
							return fmt.Sprintf("%0.0f", vf)
						}
						return ""
					},
				},
				YAxis: chart.YAxis{
					Name: "Drop (in)",
					ValueFormatter: func(v interface{}) string {
						if vf, isFloat := v.(float64); isFloat {
							return fmt.Sprintf("%0.0f", vf)
						}
						return ""
					},
				},
			}
			for i := 1; i < len(args); i++ {
				id, _ := strconv.Atoi(args[i])
				scenario, err := bd.GetScenario(ctx, &Scenario{Id: id})
				if err != nil {
					log.Printf("Skipping scenario %d for error: %v", id, err)
					continue
				}
				solver := ballistic.NewSolver(scenario.Rifle, scenario.Env, scenario.Load, true)
				solutionSet := solver.Generate(0, max, 1)
				drops := make([]float64, len(solutionSet))
				for i, sol := range solutionSet {
					drops[i] = sol.Drop
				}

				graph.Series = append(graph.Series,
					chart.ContinuousSeries{
						Name:    scenario.Name,
						XValues: ranges,
						YValues: drops,
					},
				)
			}
			graph.Elements = []chart.Renderable{
				chart.LegendThin(&graph),
			}

			file, err := os.Create(output)
			if err != nil {
				log.Printf("error creating file: %v", err)
				return
			}
			defer file.Close()

			err = graph.Render(chart.PNG, file)
			if err != nil {
				log.Printf("could not render graph: %v", err)
			}
		},
	})
	graphCmd.AddCommand(&cobra.Command{
		Use:   "wind <max range> <list of scenario ids>",
		Short: "Graph windage for scenarios up to range",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) < 2 {
				fmt.Println("Usage: drop <max range> <list of scenario ids>")
				return
			}
			max, _ := strconv.Atoi(args[0])

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			ranges := make([]float64, max)
			for i := 0; i < max; i++ {
				ranges[i] = float64(i)
			}

			graph := chart.Chart{
				XAxis: chart.XAxis{
					Name: "Range (yrds)",
					ValueFormatter: func(v interface{}) string {
						if vf, isFloat := v.(float64); isFloat {
							return fmt.Sprintf("%0.0f", vf)
						}
						return ""
					},
				},
				YAxis: chart.YAxis{
					Name: "Wind (in)",
					ValueFormatter: func(v interface{}) string {
						if vf, isFloat := v.(float64); isFloat {
							return fmt.Sprintf("%0.0f", vf)
						}
						return ""
					},
				},
			}
			for i := 1; i < len(args); i++ {
				id, _ := strconv.Atoi(args[i])
				scenario, err := bd.GetScenario(ctx, &Scenario{Id: id})
				if err != nil {
					log.Printf("Skipping scenario %d for error: %v", id, err)
					continue
				}
				solver := ballistic.NewSolver(scenario.Rifle, scenario.Env, scenario.Load, true)
				solutionSet := solver.Generate(0, max, 1)
				winds := make([]float64, len(solutionSet))
				for i, sol := range solutionSet {
					winds[i] = sol.Windage
				}

				graph.Series = append(graph.Series,
					chart.ContinuousSeries{
						Name:    scenario.Name,
						XValues: ranges,
						YValues: winds,
					},
				)
			}
			graph.Elements = []chart.Renderable{
				chart.LegendThin(&graph),
			}

			file, err := os.Create(output)
			if err != nil {
				log.Printf("error creating file: %v", err)
				return
			}
			defer file.Close()

			err = graph.Render(chart.PNG, file)
			if err != nil {
				log.Printf("could not render graph: %v", err)
			}
		},
	})
	graphCmd.AddCommand(&cobra.Command{
		Use:   "energy <max range> <list of scenario ids>",
		Short: "Graph windage for scenarios up to range",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) < 2 {
				fmt.Println("Usage: energy <max range> <list of scenario ids>")
				return
			}
			max, _ := strconv.Atoi(args[0])

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			ranges := make([]float64, max)
			for i := 0; i < max; i++ {
				ranges[i] = float64(i)
			}

			graph := chart.Chart{
				XAxis: chart.XAxis{
					Name: "Range (yrds)",
					ValueFormatter: func(v interface{}) string {
						if vf, isFloat := v.(float64); isFloat {
							return fmt.Sprintf("%0.0f", vf)
						}
						return ""
					},
				},
				YAxis: chart.YAxis{
					Name: "Energy (ft*lbs)",
					ValueFormatter: func(v interface{}) string {
						if vf, isFloat := v.(float64); isFloat {
							return fmt.Sprintf("%0.0f", vf)
						}
						return ""
					},
				},
			}
			for i := 1; i < len(args); i++ {
				id, _ := strconv.Atoi(args[i])
				scenario, err := bd.GetScenario(ctx, &Scenario{Id: id})
				if err != nil {
					log.Printf("Skipping scenario %d for error: %v", id, err)
					continue
				}
				solver := ballistic.NewSolver(scenario.Rifle, scenario.Env, scenario.Load, true)
				solutionSet := solver.Generate(0, max, 1)
				energies := make([]float64, len(solutionSet))
				for i, sol := range solutionSet {
					energies[i] = sol.Energy
				}

				graph.Series = append(graph.Series,
					chart.ContinuousSeries{
						Name:    scenario.Name,
						XValues: ranges,
						YValues: energies,
					},
				)
			}
			graph.Elements = []chart.Renderable{
				chart.LegendThin(&graph),
			}

			file, err := os.Create(output)
			if err != nil {
				log.Printf("error creating file: %v", err)
				return
			}
			defer file.Close()

			err = graph.Render(chart.PNG, file)
			if err != nil {
				log.Printf("could not render graph: %v", err)
			}
		},
	})
	graphCmd.AddCommand(&cobra.Command{
		Use:   "velocity <max range> <list of scenario ids>",
		Short: "Graph velocity for scenarios up to range",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) < 2 {
				fmt.Println("Usage: velocity <max range> <list of scenario ids>")
				return
			}
			max, _ := strconv.Atoi(args[0])

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			ranges := make([]float64, max)
			for i := 0; i < max; i++ {
				ranges[i] = float64(i)
			}

			graph := chart.Chart{
				XAxis: chart.XAxis{
					Name: "Range (yrds)",
					ValueFormatter: func(v interface{}) string {
						if vf, isFloat := v.(float64); isFloat {
							return fmt.Sprintf("%0.0f", vf)
						}
						return ""
					},
				},
				YAxis: chart.YAxis{
					Name: "Velocity (fps)",
					ValueFormatter: func(v interface{}) string {
						if vf, isFloat := v.(float64); isFloat {
							return fmt.Sprintf("%0.0f", vf)
						}
						return ""
					},
				},
			}
			for i := 1; i < len(args); i++ {
				id, _ := strconv.Atoi(args[i])
				scenario, err := bd.GetScenario(ctx, &Scenario{Id: id})
				if err != nil {
					log.Printf("Skipping scenario %d for error: %v", id, err)
					continue
				}
				solver := ballistic.NewSolver(scenario.Rifle, scenario.Env, scenario.Load, true)
				solutionSet := solver.Generate(0, max, 1)
				velocities := make([]float64, len(solutionSet))
				for i, sol := range solutionSet {
					velocities[i] = sol.Velocity
				}

				graph.Series = append(graph.Series,
					chart.ContinuousSeries{
						Name:    scenario.Name,
						XValues: ranges,
						YValues: velocities,
					},
				)
			}
			graph.Elements = []chart.Renderable{
				chart.LegendThin(&graph),
			}

			file, err := os.Create(output)
			if err != nil {
				log.Printf("error creating file: %v", err)
				return
			}
			defer file.Close()

			err = graph.Render(chart.PNG, file)
			if err != nil {
				log.Printf("could not render graph: %v", err)
			}
		},
	})
}

func AddSaveCommands(saveCmd *cobra.Command, bd *BallisticDatabase) {
	var output string
	saveCmd.PersistentFlags().StringVarP(&output, "output", "o", "solution.csv", "filename for graph")
	saveCmd.ParseFlags(os.Args)

	saveCmd.AddCommand(&cobra.Command{
		Use:   "csv <scenario id> <min> <max> <increment>",
		Short: "Save table to csv",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 4 {
				fmt.Println("Usage: csv <scenario id> <min> <max> <increment>")
				return
			}
			scenario_id, _ := strconv.Atoi(args[0])
			min, _ := strconv.Atoi(args[1])
			max, _ := strconv.Atoi(args[2])
			inc, _ := strconv.Atoi(args[3])

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			scenario, err := bd.GetScenario(ctx, &Scenario{Id: scenario_id})
			if err != nil {
				log.Printf("Error getting scenario: %v", err)
				return
			}

			solver := ballistic.NewSolver(scenario.Rifle, scenario.Env, scenario.Load, true)
			solver.SaveToCsv(output, min, max, inc)
			fmt.Printf("Solution saved to '%s'\n", output)
		},
	})
	saveCmd.AddCommand(&cobra.Command{
		Use:   "xlsx <scenario id> <min> <max> <increment>",
		Short: "Save table to xlsx",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 4 {
				fmt.Println("Usage: csv <scenario id> <min> <max> <increment>")
				return
			}
			scenario_id, _ := strconv.Atoi(args[0])
			min, _ := strconv.Atoi(args[1])
			max, _ := strconv.Atoi(args[2])
			inc, _ := strconv.Atoi(args[3])

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			scenario, err := bd.GetScenario(ctx, &Scenario{Id: scenario_id})
			if err != nil {
				log.Printf("Error getting scenario: %v", err)
				return
			}

			solver := ballistic.NewSolver(scenario.Rifle, scenario.Env, scenario.Load, true)
			solver.SaveToXlsx(output, min, max, inc)
			fmt.Printf("Solution saved to '%s'\n", output)
		},
	})
}

func DatabaseSolveCommands(solveCmd *cobra.Command, bd *BallisticDatabase) {
	solveCmd.AddCommand(&cobra.Command{
		Use:   "table <scenario_id> <min> <max> <increment>",
		Short: "Solution table for the given scenario",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 4 {
				fmt.Println("table <scenario_id> <min> <max> <increment>")
				return
			}
			scenario_id, _ := strconv.Atoi(args[0])
			min, _ := strconv.Atoi(args[1])
			max, _ := strconv.Atoi(args[2])
			inc, _ := strconv.Atoi(args[3])

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			scenario, err := bd.GetScenario(ctx, &Scenario{Id: scenario_id})
			if err != nil {
				log.Printf("Error getting scenario: %v", err)
				return
			}

			solver := ballistic.NewSolver(scenario.Rifle, scenario.Env, scenario.Load, true)
			solutions := solver.Generate(min, max, inc)
			for _, sol := range solutions {
				sol.Drop = Round(sol.Drop, 2)
				sol.DropMOA = Round(sol.DropMOA, 2)
				sol.Energy = Round(sol.Energy, 2)
				sol.Velocity = Round(sol.Velocity, 2)
				sol.VelocityX = Round(sol.VelocityX, 2)
				sol.VelocityY = Round(sol.VelocityY, 2)
				sol.WindageMOA = Round(sol.WindageMOA, 2)
				sol.Windage = Round(sol.Windage, 2)
				sol.Time = Round(sol.Time, 3)

			}
			text, err := tabulate.Tabulate(
				solutions, &tabulate.Layout{Format: tabulate.SimpleFormat},
			)
			if err != nil {
				log.Printf("tabulate error: %v", err)
				return
			}
			fmt.Print(text)
		},
	})
	solveCmd.AddCommand(&cobra.Command{
		Use:   "range <scenario id> <range(yrds)>",
		Short: "Solve for the given range",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 2 {
				log.Printf("Bad command, do it right")
				return
			}
			scenario_id, _ := strconv.Atoi(args[0])
			rng, _ := strconv.Atoi(args[1])

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			scenario, err := bd.GetScenario(ctx, &Scenario{Id: scenario_id})
			if err != nil {
				log.Printf("Error getting scenario: %v", err)
				return
			}

			solver := ballistic.NewSolver(scenario.Rifle, scenario.Env, scenario.Load, true)
			solution, err := solver.SolveFor(rng)
			if err != nil {
				log.Printf("there was an error: %v", err)
				return
			}
			fmt.Println("Range\tRange\tDropMOA\tWindageMOA\tVelocity\tEnergy")
			fmt.Printf("%d\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\n",
				solution.Range,
				solution.RawRange,
				solution.DropMOA,
				solution.WindageMOA,
				solution.Velocity,
				solution.Energy,
			)
		},
	})
}

func DatabaseCreateCommands(createCmd *cobra.Command, bd *BallisticDatabase) {
	createCmd.AddCommand(&cobra.Command{
		Use:   "rifle",
		Short: "Create rifle",
		Run: func(cmd *cobra.Command, args []string) {
			rifle := &ballistic.Rifle{}
			scanner := bufio.NewScanner(os.Stdin)
			fmt.Printf("Name: ")
			scanner.Scan()
			rifle.Name = scanner.Text()
			fmt.Printf("Sight Height (in): ")
			fmt.Scanln(&rifle.SightHeight)
			fmt.Printf("Barrel Twist (in): ")
			fmt.Scanln(&rifle.BarrelTwist)
			fmt.Printf("Twist Direction Left (true/false): ")
			fmt.Scanln(&rifle.TwistDirectionLeft)
			fmt.Printf("Zero Range (yrds): ")
			fmt.Scanln(&rifle.ZeroRange)

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			r, err := bd.CreateRifle(ctx, rifle)
			if err != nil {
				log.Printf("could not create rifle: %v", err)
				return
			}
			fmt.Println("ID: ", r.Id)
		},
	})
	createCmd.AddCommand(&cobra.Command{
		Use:   "environment",
		Short: "Create environment",
		Run: func(cmd *cobra.Command, args []string) {
			env := &ballistic.Environment{}
			fmt.Printf("Temperature (F): ")
			fmt.Scanln(&env.Temperature)
			fmt.Printf("Altitude (ft): ")
			fmt.Scanln(&env.Altitude)
			fmt.Printf("Pressure (hgP): ")
			fmt.Scanln(&env.Pressure)
			fmt.Printf("Humidity (%%): ")
			fmt.Scanln(&env.Humidity)
			fmt.Printf("WindAngle (deg): ")
			fmt.Scanln(&env.WindAngle)
			fmt.Printf("WindSpeed (mph): ")
			fmt.Scanln(&env.WindSpeed)
			fmt.Printf("PressureIsAbsolute (true/false): ")
			fmt.Scanln(&env.PressureIsAbsolute)
			fmt.Printf("Latitude: ")
			fmt.Scanln(&env.Latitude)
			fmt.Printf("Azimuth (deg): ")
			fmt.Scanln(&env.Azimuth)

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			e, err := bd.CreateEnvironment(ctx, env)
			if err != nil {
				log.Printf("could not create environment: %v", err)
				return
			}
			fmt.Println(e)
		},
	})
	createCmd.AddCommand(&cobra.Command{
		Use:   "load",
		Short: "Create load",
		Run: func(cmd *cobra.Command, args []string) {
			load := &ballistic.Load{
				Bullet: &ballistic.Bullet{
					BC: &ballistic.BallisticCoefficient{},
				},
			}
			fmt.Printf("Caliber (in): ")
			fmt.Scanln(&load.Bullet.Caliber)
			fmt.Printf("Weight (gr): ")
			fmt.Scanln(&load.Bullet.Weight)
			fmt.Printf("Drag Function (G1..G7): ")
			fmt.Scanln(&load.Bullet.BC.DragFunc)
			fmt.Printf("Ballistic Coefficient: ")
			fmt.Scanln(&load.Bullet.BC.Value)
			fmt.Printf("Length (in): ")
			fmt.Scanln(&load.Bullet.Length)
			fmt.Printf("Muzzle Velocity (fps): ")
			fmt.Scanln(&load.MuzzleVelocity)

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			l, err := bd.CreateLoad(ctx, load)
			if err != nil {
				log.Printf("could not creating load: %v", err)
				return
			}
			fmt.Println("ID: ", l.Id)
		},
	})
	createCmd.AddCommand(&cobra.Command{
		Use:   "scenario",
		Short: "Create scenario",
		Run: func(cmd *cobra.Command, args []string) {
			scenario := &Scenario{
				Env:   &ballistic.Environment{},
				Rifle: &ballistic.Rifle{},
				Load:  &ballistic.Load{},
			}
			scanner := bufio.NewScanner(os.Stdin)
			fmt.Printf("Scenario Name: ")
			scanner.Scan()
			scenario.Name = scanner.Text()
			fmt.Printf("Environment Id: ")
			fmt.Scanln(&scenario.Env.Id)
			fmt.Printf("Rifle Id: ")
			fmt.Scanln(&scenario.Rifle.Id)
			fmt.Printf("Load Id: ")
			fmt.Scanln(&scenario.Load.Id)

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			s, err := bd.CreateScenario(ctx, scenario)
			if err != nil {
				log.Printf("could not creating load: %v", err)
				return
			}
			fmt.Println("Scenario Id: ", s.Id)
		},
	})
}

func DatabaseListCommands(listCmd *cobra.Command, bd *BallisticDatabase) {
	listCmd.AddCommand(&cobra.Command{
		Use:   "rifles",
		Short: "List rifles in database",
		Run: func(cmd *cobra.Command, args []string) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			rifles, err := bd.ListRifles(ctx)
			if err != nil {
				log.Printf("Error listing rifles: %v", err)
				return
			}
			text, err := tabulate.Tabulate(
				rifles, &tabulate.Layout{Format: tabulate.SimpleFormat},
			)
			if err != nil {
				log.Printf("tabulate error: %v", err)
				return
			}
			fmt.Print(text)
		},
	})

	listCmd.AddCommand(&cobra.Command{
		Use:   "environments",
		Short: "List environments in database",
		Run: func(cmd *cobra.Command, args []string) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			list, err := bd.ListEnvironments(ctx)
			if err != nil {
				log.Printf("Error listing rifles: %v", err)
				return
			}
			text, err := tabulate.Tabulate(
				list, &tabulate.Layout{Format: tabulate.SimpleFormat},
			)
			if err != nil {
				log.Printf("tabulate error: %v", err)
				return
			}
			fmt.Print(text)
		},
	})

	listCmd.AddCommand(&cobra.Command{
		Use:   "loads",
		Short: "List loads in database",
		Run: func(cmd *cobra.Command, args []string) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			loads, err := bd.ListLoads(ctx)
			if err != nil {
				log.Printf("Error listing loads: %v", err)
				return
			}
			vals := make([]*LoadPrintable, len(loads))
			for i, load := range loads {
				vals[i] = NewLoadPrintable(load)
			}
			text, err := tabulate.Tabulate(
				vals, &tabulate.Layout{Format: tabulate.SimpleFormat},
			)
			if err != nil {
				log.Printf("tabulate error: %v", err)
				return
			}
			fmt.Print(text)
		},
	})

	listCmd.AddCommand(&cobra.Command{
		Use:   "scenarios",
		Short: "List scenarios in database",
		Run: func(cmd *cobra.Command, args []string) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			scenarios, err := bd.ListScenarios(ctx)
			if err != nil {
				log.Printf("Error listing loads: %v", err)
				return
			}
			// Make Printable
			vals := make([]*ScenarioPrintable, len(scenarios))
			for i, s := range scenarios {
				vals[i] = NewScenarioPrintable(s)
			}
			// Print
			text, err := tabulate.Tabulate(
				vals, &tabulate.Layout{Format: tabulate.SimpleFormat},
			)
			if err != nil {
				log.Printf("tabulate error: %v", err)
				return
			}
			fmt.Print(text)
		},
	})

}
