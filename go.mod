module gitlab.com/linuxfreak003/go-ballistic

go 1.15

require (
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/rgeoghegan/tabulate v0.0.0-20170527210506-f65c88667bb4
	github.com/spf13/cobra v1.1.3
	github.com/stretchr/testify v1.6.1
	github.com/wcharczuk/go-chart/v2 v2.1.0
	gitlab.com/linuxfreak003/ballistic v1.0.2
)
