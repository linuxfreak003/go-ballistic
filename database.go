package main

import (
	"context"
	"database/sql"
	"fmt"

	"gitlab.com/linuxfreak003/ballistic"
)

type BallisticDatabase struct {
	Db *sql.DB
}

type Scenario struct {
	Id    int
	Name  string
	Env   *ballistic.Environment
	Rifle *ballistic.Rifle
	Load  *ballistic.Load
}

func NewBallisticDatabase(db *sql.DB) *BallisticDatabase {
	return &BallisticDatabase{
		Db: db,
	}
}

func (b *BallisticDatabase) CreateScenario(ctx context.Context, s *Scenario) (*Scenario, error) {
	if s.Env == nil || s.Rifle == nil || s.Load == nil {
		return nil, fmt.Errorf("missing env, rifle, or load")
	}
	var err error
	s.Env, err = b.GetEnvironment(ctx, s.Env)
	if err != nil {
		return nil, err
	}
	s.Rifle, err = b.GetRifle(ctx, s.Rifle)
	if err != nil {
		return nil, err
	}
	s.Load, err = b.GetLoad(ctx, s.Load)
	if err != nil {
		return nil, err
	}

	query := `INSERT INTO scenarios (
		name,
		environment_id,
		rifle_id,
		load_id
	) VALUES (?,?,?,?);`
	r, err := b.Db.ExecContext(ctx, query,
		s.Name,
		s.Env.Id,
		s.Rifle.Id,
		s.Load.Id,
	)
	if err != nil {
		return nil, fmt.Errorf("could not execute query: %v", err)
	}
	id, err := r.LastInsertId()
	if err != nil {
		return nil, fmt.Errorf("could not get last insert id: %v", err)
	}
	s.Id = int(id)

	return s, nil
}
func (b *BallisticDatabase) ListScenarios(ctx context.Context) ([]*Scenario, error) {
	query := `SELECT
		id,
		name,
		environment_id,
		rifle_id,
		load_id
	FROM scenarios;`
	scenarios := []*Scenario{}
	rows, err := b.Db.QueryContext(ctx, query)
	if err != nil {
		return nil, fmt.Errorf("could not execute query: %v", err)
	}
	for rows.Next() {
		scenario := &Scenario{
			Env:   &ballistic.Environment{},
			Rifle: &ballistic.Rifle{},
			Load:  &ballistic.Load{},
		}
		rows.Scan(
			&scenario.Id,
			&scenario.Name,
			&scenario.Env.Id,
			&scenario.Rifle.Id,
			&scenario.Load.Id,
		)
		scenarios = append(scenarios, scenario)
	}
	return scenarios, nil
}
func (b *BallisticDatabase) GetScenario(ctx context.Context, s *Scenario) (*Scenario, error) {
	if s.Env == nil {
		s.Env = &ballistic.Environment{}
	}
	if s.Rifle == nil {
		s.Rifle = &ballistic.Rifle{}
	}
	if s.Load == nil {
		s.Load = &ballistic.Load{}
	}
	query := `SELECT
		id,
		name,
		environment_id,
		rifle_id,
		load_id
	FROM scenarios
	WHERE id == ?;`
	row := b.Db.QueryRowContext(ctx, query, s.Id)
	err := row.Scan(
		&s.Id,
		&s.Name,
		&s.Env.Id,
		&s.Rifle.Id,
		&s.Load.Id,
	)
	if err != nil {
		return nil, fmt.Errorf("could not execute query: %v", err)
	}

	s.Env, err = b.GetEnvironment(ctx, s.Env)
	if err != nil {
		return nil, err
	}
	s.Rifle, err = b.GetRifle(ctx, s.Rifle)
	if err != nil {
		return nil, err
	}
	s.Load, err = b.GetLoad(ctx, s.Load)
	if err != nil {
		return nil, err
	}

	return s, nil
}

func (b *BallisticDatabase) CreateEnvironment(ctx context.Context, env *ballistic.Environment) (*ballistic.Environment, error) {
	query := `INSERT INTO environments (
		temperature,
		altitude,
		pressure,
		humidity,
		wind_angle,
		wind_speed,
		pressure_is_absolute,
		latitude,
		azimuth
	) VALUES (?,?,?,?,?,?,?,?,?);`
	r, err := b.Db.ExecContext(ctx, query,
		env.Temperature,
		env.Altitude,
		env.Pressure,
		env.Humidity,
		env.WindAngle,
		env.WindSpeed,
		env.PressureIsAbsolute,
		env.Latitude,
		env.Azimuth,
	)
	if err != nil {
		return nil, fmt.Errorf("could not execute query: %v", err)
	}
	id, err := r.LastInsertId()
	if err != nil {
		return nil, fmt.Errorf("could not get last insert id: %v", err)
	}
	env.Id = int(id)
	return env, nil
}
func (b *BallisticDatabase) ListEnvironments(ctx context.Context) ([]*ballistic.Environment, error) {
	query := `SELECT
		id,
		temperature,
		altitude,
		pressure,
		humidity,
		wind_angle,
		wind_speed,
		pressure_is_absolute,
		latitude,
		azimuth
	FROM environments;`
	environments := []*ballistic.Environment{}
	rows, err := b.Db.QueryContext(ctx, query)
	if err != nil {
		return nil, fmt.Errorf("could not execute query: %v", err)
	}
	for rows.Next() {
		env := &ballistic.Environment{}
		rows.Scan(
			&env.Id,
			&env.Temperature,
			&env.Altitude,
			&env.Pressure,
			&env.Humidity,
			&env.WindAngle,
			&env.WindSpeed,
			&env.PressureIsAbsolute,
			&env.Latitude,
			&env.Azimuth,
		)
		environments = append(environments, env)
	}
	return environments, nil
}
func (b *BallisticDatabase) UpdateEnvironment(ctx context.Context, env *ballistic.Environment) (*ballistic.Environment, error) {
	query := `UPDATE environments SET
		temperature = ?,
		altitude = ?,
		pressure = ?,
		humidity = ?,
		wind_angle = ?,
		wind_speed = ?,
		pressure_is_absolute = ?,
		latitude = ?,
		azimuth = ?
	WHERE id = ?`
	r, err := b.Db.ExecContext(ctx, query,
		env.Temperature,
		env.Altitude,
		env.Pressure,
		env.Humidity,
		env.WindAngle,
		env.WindSpeed,
		env.PressureIsAbsolute,
		env.Latitude,
		env.Azimuth,
		env.Id,
	)
	if err != nil {
		return nil, fmt.Errorf("could not execute update query: %v", err)
	}
	if affected, err := r.RowsAffected(); err != nil || affected != 1 {
		return nil, fmt.Errorf("rows affected error or not 1: %v", err)
	}
	return env, nil
}
func (b *BallisticDatabase) DeleteEnvironment(ctx context.Context, env *ballistic.Environment) error {
	query := `DELETE FROM environments WHERE id = ?`
	r, err := b.Db.ExecContext(ctx, query, env.Id)
	if err != nil {
		return fmt.Errorf("could not execute query: %v", err)
	}
	if affected, err := r.RowsAffected(); err != nil || affected != 1 {
		return fmt.Errorf("rows affected error or not 1: %v", err)
	}
	return nil
}
func (b *BallisticDatabase) GetEnvironment(ctx context.Context, env *ballistic.Environment) (*ballistic.Environment, error) {
	query := `SELECT
		id,
		temperature,
		altitude,
		pressure,
		humidity,
		wind_angle,
		wind_speed,
		pressure_is_absolute,
		latitude,
		azimuth
	FROM environments WHERE id = ?;`
	row := b.Db.QueryRowContext(ctx, query, env.Id)
	err := row.Scan(
		&env.Id,
		&env.Temperature,
		&env.Altitude,
		&env.Pressure,
		&env.Humidity,
		&env.WindAngle,
		&env.WindSpeed,
		&env.PressureIsAbsolute,
		&env.Latitude,
		&env.Azimuth,
	)
	if err != nil {
		return nil, fmt.Errorf("could not scan environment: %v", err)
	}
	return env, nil
}

func (b *BallisticDatabase) CreateRifle(ctx context.Context, rifle *ballistic.Rifle) (*ballistic.Rifle, error) {
	query := `INSERT INTO rifles (
		name,
		sight_height,
		barrel_twist,
		twist_direction_left,
		zero_range
	) VALUES (?,?,?,?,?);`
	r, err := b.Db.ExecContext(ctx, query,
		rifle.Name,
		rifle.SightHeight,
		rifle.BarrelTwist,
		rifle.TwistDirectionLeft,
		rifle.ZeroRange,
	)
	if err != nil {
		return nil, fmt.Errorf("could not execute query: %v", err)
	}
	id, err := r.LastInsertId()
	if err != nil {
		return nil, fmt.Errorf("could not get last insert id: %v", err)
	}
	rifle.Id = int(id)
	return rifle, nil
}
func (b *BallisticDatabase) ListRifles(ctx context.Context) ([]*ballistic.Rifle, error) {
	query := `SELECT
		id,
		name,
		sight_height,
		barrel_twist,
		twist_direction_left,
		zero_range
	FROM rifles;`
	rifles := []*ballistic.Rifle{}
	rows, err := b.Db.QueryContext(ctx, query)
	if err != nil {
		return nil, fmt.Errorf("could not execute query: %v", err)
	}
	for rows.Next() {
		rifle := &ballistic.Rifle{}
		rows.Scan(
			&rifle.Id,
			&rifle.Name,
			&rifle.SightHeight,
			&rifle.BarrelTwist,
			&rifle.TwistDirectionLeft,
			&rifle.ZeroRange,
		)
		rifles = append(rifles, rifle)
	}
	return rifles, nil
}
func (b *BallisticDatabase) UpdateRifle(ctx context.Context, rifle *ballistic.Rifle) (*ballistic.Rifle, error) {
	return nil, fmt.Errorf("unimplemented")
}
func (b *BallisticDatabase) DeleteRifle(ctx context.Context, rifle *ballistic.Rifle) error {
	return fmt.Errorf("unimplemented")
}
func (b *BallisticDatabase) GetRifle(ctx context.Context, rifle *ballistic.Rifle) (*ballistic.Rifle, error) {
	query := `SELECT
		id,
		name,
		sight_height,
		barrel_twist,
		twist_direction_left,
		zero_range
	FROM rifles WHERE id = ?;`
	row := b.Db.QueryRowContext(ctx, query, rifle.Id)
	err := row.Scan(
		&rifle.Id,
		&rifle.Name,
		&rifle.SightHeight,
		&rifle.BarrelTwist,
		&rifle.TwistDirectionLeft,
		&rifle.ZeroRange,
	)
	if err != nil {
		return nil, fmt.Errorf("could not scan rifle: %v", err)
	}
	return rifle, nil
}

func (b *BallisticDatabase) CreateLoad(ctx context.Context, load *ballistic.Load) (*ballistic.Load, error) {
	query := `INSERT INTO loads (
		bullet_caliber,
		bullet_weight,
		bullet_bc_value,
		bullet_bc_drag_func,
		bullet_length,
		muzzle_velocity
	) VALUES (?,?,?,?,?,?);`
	r, err := b.Db.ExecContext(ctx, query,
		load.Bullet.Caliber,
		load.Bullet.Weight,
		load.Bullet.BC.Value,
		load.Bullet.BC.DragFunc,
		load.Bullet.Length,
		load.MuzzleVelocity,
	)
	if err != nil {
		return nil, fmt.Errorf("could not execute query: %v", err)
	}
	id, err := r.LastInsertId()
	if err != nil {
		return nil, fmt.Errorf("could not get last insert id: %v", err)
	}
	load.Id = int(id)
	return load, nil
}
func (b *BallisticDatabase) ListLoads(ctx context.Context) ([]*ballistic.Load, error) {
	query := `SELECT
		id,
		bullet_caliber,
		bullet_weight,
		bullet_bc_value,
		bullet_bc_drag_func,
		bullet_length,
		muzzle_velocity
	FROM loads;`
	loads := []*ballistic.Load{}
	rows, err := b.Db.QueryContext(ctx, query)
	if err != nil {
		return nil, fmt.Errorf("could not execute query: %v", err)
	}
	for rows.Next() {
		load := &ballistic.Load{
			Bullet: &ballistic.Bullet{
				BC: &ballistic.BallisticCoefficient{},
			},
		}
		rows.Scan(
			&load.Id,
			&load.Bullet.Caliber,
			&load.Bullet.Weight,
			&load.Bullet.BC.Value,
			&load.Bullet.BC.DragFunc,
			&load.Bullet.Length,
			&load.MuzzleVelocity,
		)
		loads = append(loads, load)
	}
	return loads, nil
}
func (b *BallisticDatabase) UpdateLoad(ctx context.Context, load *ballistic.Load) (*ballistic.Load, error) {
	return nil, fmt.Errorf("unimplemented")
}
func (b *BallisticDatabase) DeleteLoad(ctx context.Context, load *ballistic.Load) error {
	return fmt.Errorf("unimplemented")
}
func (b *BallisticDatabase) GetLoad(ctx context.Context, load *ballistic.Load) (*ballistic.Load, error) {
	if load.Bullet == nil {
		load.Bullet = &ballistic.Bullet{
			BC: &ballistic.BallisticCoefficient{},
		}
	}
	if load.Bullet.BC == nil {
		load.Bullet.BC = &ballistic.BallisticCoefficient{}
	}
	query := `SELECT
		id,
		bullet_caliber,
		bullet_weight,
		bullet_bc_value,
		bullet_bc_drag_func,
		bullet_length,
		muzzle_velocity
	FROM loads WHERE id = ?;`
	row := b.Db.QueryRowContext(ctx, query, load.Id)
	err := row.Scan(
		&load.Id,
		&load.Bullet.Caliber,
		&load.Bullet.Weight,
		&load.Bullet.BC.Value,
		&load.Bullet.BC.DragFunc,
		&load.Bullet.Length,
		&load.MuzzleVelocity,
	)
	if err != nil {
		return nil, fmt.Errorf("could not scan load: %v", err)
	}
	return load, nil
}

// TODO: Add contraints
func (b *BallisticDatabase) SetupDatabase(ctx context.Context) error {
	query := `CREATE TABLE IF NOT EXISTS rifles (
		id INTEGER PRIMARY KEY,
		name TEXT,
		sight_height FLOAT,
		barrel_twist FLOAT,
		twist_direction_left BOOL,
		zero_range FLOAT
	)`
	_, err := b.Db.ExecContext(ctx, query)
	if err != nil {
		return err
	}

	query = `CREATE TABLE IF NOT EXISTS loads (
		id INTEGER PRIMARY KEY,
		bullet_caliber FLOAT,
		bullet_weight FLOAT,
		bullet_bc_value FLOAT,
		bullet_bc_drag_func INTEGER,
		bullet_length FLOAT,
		muzzle_velocity FLOAT
	)`
	_, err = b.Db.ExecContext(ctx, query)
	if err != nil {
		return err
	}

	query = `CREATE TABLE IF NOT EXISTS environments (
		id INTEGER PRIMARY KEY,
		temperature FLOAT,
		altitude INTEGER,
		pressure FLOAT,
		humidity FLOAT,
		wind_angle FLOAT,
		wind_speed FLOAT,
		pressure_is_absolute BOOL,
		latitude FLOAT,
		azimuth FLOAT
	)`
	_, err = b.Db.ExecContext(ctx, query)
	if err != nil {
		return err
	}

	query = `CREATE TABLE IF NOT EXISTS scenarios (
		id INTEGER PRIMARY KEY,
		name TEXT,
		environment_id INTEGER,
		rifle_id INTEGER,
		load_id INTEGER
	)`
	_, err = b.Db.ExecContext(ctx, query)
	if err != nil {
		return err
	}
	return nil
}
