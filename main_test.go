package main_test

import (
	"fmt"
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/linuxfreak003/ballistic"
)

func TestScenario(t *testing.T) {
	t.Run("NewScenario", func(t *testing.T) {
		s := ballistic.NewSolver(nil, nil, nil, false)
		assert.NotNil(t, s)
	})
	s := &ballistic.Solver{
		Rifle: &ballistic.Rifle{
			Name:        "308 Win",
			SightHeight: 1.5,
			BarrelTwist: 7.0,
			ZeroRange:   200,
		},
		Environment: &ballistic.Environment{
			Pressure:           29.53,
			PressureIsAbsolute: true,
		},
		Load: &ballistic.Load{
			Bullet: &ballistic.Bullet{
				Caliber: .308,
				Weight:  208,
				BC: &ballistic.BallisticCoefficient{
					Value:    .250,
					DragFunc: ballistic.G7,
				},
				Length: 1.2,
			},
			MuzzleVelocity: 2900.0,
		},
		IncludeSpinDrift: true,
	}
	t.Run("SolveFor", func(t *testing.T) {
		sln, err := s.SolveFor(200)
		assert.Nil(t, err)
		assert.Equal(t, 0.0, math.Round(sln.DropMOA))
	})
	t.Run("SolveSubsonic", func(t *testing.T) {
		sln, err := s.SolveSubsonic()
		assert.Nil(t, err)
		assert.Less(t, sln.Velocity, 1200.0)
	})
	t.Run("Generate", func(t *testing.T) {
		slns := s.Generate(0, 500, 25)
		fmt.Println("Range:\tRange\tDropMOA\tWindageMOA\tVelocity\tEnergy")
		for _, s := range slns {
			fmt.Printf("%d\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\n",
				s.Range,
				s.RawRange,
				s.DropMOA,
				s.WindageMOA,
				s.Velocity,
				s.Energy,
			)
		}
	})
}
